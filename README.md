# Mex

Mex is a **M**ath **ex**pression solver that - as the name implies - solves mathematical equations.

For now it only supports the following operators, ordered by precedence (highest to lowest):

- `( ... )`: Create a sub-expression
- `*`, `/`: Multiplication, Division
- `+`, `-`: Addition, Subtraction

> Note: Sub-expressions (enclosed in `( )`) are always evaluated first.

## Try it

> Make sure that you have the [Rust toolchain](https://www.rust-lang.org/) installed.

Clone the repository

```sh
git clone https://gitlab.com/0xIsho/Mex.git
```

Build

```sh
cd Mex

cargo build --release
```

Test

```sh
cargo test
```

Run

```sh
target/release/mex '1 + 1'
# 2
target/release/mex '(2 + 2) * 3'
# 12
```

## How it works

### In Theory
Each expression is made up of an *operator* and one or two *operands*.

The following is an expression:
```
1
```
Which can also be represented as `Unary(1)` (meaning a [unary operator](https://en.wikipedia.org/wiki/Unary_operation)); evaluates to the value `1`.

Another (a bit more complex) one:
```
1 + 2
```
Is made up of an operator (`+`) and two operands (`1` and `2`).
It can be represented as `Binary(Fn(+), Unary(1), Unary(2))`
( meaning a [binary operator](https://en.wikipedia.org/wiki/Binary_operation)
with a function (addition in this case) and two Unary operands)

Expressions can be nested:
```
1 + 2 + 3
```
This statement is equal to (among other ways to nest them):
`(1 + 2) + (3)`. `(1 + 2)` is an expression,
and `<expr-result> + 3` being the second one.

The representation would be `Binary(Fn(+), Binary(Fn(+), Unary(1), Unary(2)), Unary(3))`

Parentheses (`()`) can be used to explicitly group operands into an expression:
```
(3 * (1 + 1)) / 2
```
`Binary(Fn(/), Binary(Fn(*), Unary(3), Binary(Fn(+), Unary(1), Unary(1))), Unary(2))`

After building the expression tree, solving it is as simple as recursively evaluating the operators.

### In Practice

1. The input string is [tokenized](https://en.wikipedia.org/wiki/Lexical_analysis#Tokenization).
A token is either one of the supported operators,
or a numerical value (an error is thrown in case it's neither!).

   Given the following input:
   ```
   2 * 3
   ```
   The (tokenized) output will be:
   ```
   ["2", "*", "3"]
   ```

1. Which is then converted to
   [postfix](https://en.wikipedia.org/wiki/Reverse_Polish_notation) notation.

   ```
   ["2", "3", "*"]
   ```

1. The operators tree is built based on the postfix expression

   ```
   [Binary(Fn(*), Unary(2), Unary(3))]
   ```

1. The tree is evaluated (recursively)

   ```
   Fn(*)(Unary(2), Unary(3)) -> Fn(*)(2, 3) -> 6
   ```

## Known issues

- The calculator does not handle the negation operator.
    The `-` in `-1` is treated as a binary operator.
- It does not include some nice-to-have operators. e.g:
  - `x ^ y`: `x` raised to the power `y`
  - `x % y`: The modulo operator. Divides `x` by `y` and returns the reminder
- Conversion operators for different bases
  - Literals:
    - `0b???`: Treat `???` as a binary (base 2) number
    - `0x???`: Treat `???` as a hex (base 16) value
    - `0????`: Treat `????` as an oct (base 8) value
    - ...
  - Functions:
    - `bin(???)`: `???` is binary
    - `oct(???)`: `???` is octal
    - `hex(???)`: `???` is hexadecimal
    - ...
- It's not a crate, so cannot be used in other projects.
- Lacks tests. We need *more*!
- Variables? \*wink\* \*wink\*

## Contributing

I made this project mainly for practice, as i'm (trying :D) to learn rust,
so it's likely that the code has issues that (as a rust beginner)
i'm not aware of; It might also have bugs, optimization opportunities, or otherwise.

Feel free to submit patches fixing/adding stuff. MRs welcome!

## LICENSE

This project is licensed under the MIT license.
See [LICENSE](LICENSE) for details.
