// SPDX-License-Identifier: MIT

struct Unary {
    operand: String,
}

type OpFn = fn(f64, f64) -> Box<dyn Expr>;

struct Binary {
    op: OpFn,
    lhs: Box<dyn Expr>,
    rhs: Box<dyn Expr>,
}

trait Expr {
    fn eval(&self) -> f64;
}

impl Expr for Unary {
    fn eval(&self) -> f64 {
        self.operand.parse::<f64>().unwrap() // TODO: Error handling
    }
}

impl Expr for Binary {
    fn eval(&self) -> f64 {
        (self.op)(self.lhs.eval(), self.rhs.eval()).eval()
    }
}

struct Token;

impl Token {
    /* Operator | Precedence | Function */
    const OPERATORS: [(&'static str, u32, OpFn); 6] = [
        ("*", 3, |x: f64, y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (x * y).to_string(),
            })
        }),
        ("/", 3, |x: f64, y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (x / y).to_string(),
            })
        }),
        ("+", 2, |x: f64, y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (x + y).to_string(),
            })
        }),
        ("-", 2, |x: f64, y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (x - y).to_string(),
            })
        }),
        ("(", 1, |_x: f64, _y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (0).to_string(),
            })
        }),
        (")", 1, |_x: f64, _y: f64| -> Box<dyn Expr> {
            Box::new(Unary {
                operand: (0).to_string(),
            })
        }),
    ];

    fn is_operator(val: &str) -> bool {
        for (key, _value, _f) in Token::OPERATORS {
            if key == val {
                return true;
            }
        }

        false
    }

    fn operator_precedence(op: &str) -> u32 {
        for (key, value, _f) in Token::OPERATORS {
            if op == key {
                return value;
            }
        }

        panic!("Unknown operator: {}", op);
    }

    fn op_fn(op: &str) -> fn(f64, f64) -> Box<dyn Expr> {
        for (key, _val, f) in Token::OPERATORS {
            if op == key {
                return f;
            }
        }

        panic!("Unknown operator: {}", op);
    }

    fn is_numeric(val: &str) -> bool {
        let mut found_dec_point = false;

        for c in val.chars() {
            if c == '.' {
                if found_dec_point {
                    return false;
                }

                found_dec_point = true;
                continue;
            } else if !c.is_numeric() {
                return false;
            }
        }

        true
    }
}

fn scan(input: &str) -> Vec<String> {
    let mut tokens = Vec::<String>::new();

    let mut tmp_tok = String::new();
    for ch in input.chars() {
        if ch.is_whitespace() {
            continue;
        }

        let tmp = ch.to_string();

        if Token::is_numeric(&tmp) {
            tmp_tok.push(ch);
        } else if Token::is_operator(&tmp) {
            if !tmp_tok.is_empty() {
                tokens.push(tmp_tok.clone());
                tmp_tok.clear();
            }

            tokens.push(tmp);
        } else {
            panic!("Unexpected token: {}", ch);
        }
    }

    if !tmp_tok.is_empty() {
        tokens.push(tmp_tok);
    }

    tokens
}

fn infix_to_postfix(mut tokens: Vec<String>) -> Vec<String> {
    let mut op_stack = Vec::<String>::new();
    let mut postfix = Vec::<String>::new();

    op_stack.push("(".to_string()); // Begin an expression
    tokens.push(")".to_string()); // Expression is the full statement for now.

    for elem in tokens.iter() {
        if elem == "(" {
            op_stack.push(elem.to_string());
        } else if elem == ")" {
            let mut tmp = op_stack.pop().unwrap();
            while tmp != "(" {
                postfix.push(tmp);
                tmp = op_stack.pop().unwrap();
            }
        } else if Token::is_operator(elem) {
            let op_prec = Token::operator_precedence(elem);
            let mut top = op_stack.last().unwrap();

            // Pop operators with higher precedence than the current one
            // and push then into the expression
            while Token::operator_precedence(top) > op_prec {
                postfix.push(op_stack.pop().unwrap());
                top = match op_stack.last() {
                    None => {
                        break;
                    }
                    _ => elem,
                }
            }
            op_stack.push(elem.to_string());
        } else {
            // Valid operand; Assert it?
            postfix.push(elem.to_string());
        }
    }

    postfix
}

fn parse(tokens: Vec<String>) -> Vec<Box<dyn Expr>> {
    let postfix = infix_to_postfix(tokens);

    let mut operators = Vec::<Box<dyn Expr>>::new();

    for tok in postfix {
        if Token::is_operator(&tok) {
            let rhs = operators.pop().unwrap();
            let lhs = operators.pop().unwrap();

            operators.push(Box::new(Binary {
                op: Token::op_fn(&tok),
                lhs,
                rhs,
            }));
        } else if Token::is_numeric(&tok) {
            operators.push(Box::new(Unary { operand: tok }));
        } else {
            panic!("Invalid token: {}", tok);
        }
    }

    operators
}

fn eval(input: &str) -> f64 {
    parse(scan(input))[0].eval()
}

fn usage() {
    println!("Usage: {} '<expr>'", std::env::args().next().unwrap());
}

fn main() {
    if std::env::args().len() != 2 {
        println!("Error: Invalid arguments.");
        usage();
        std::process::exit(1);
    }

    let mut input: String = String::new();

    for arg in std::env::args() {
        match arg.as_str() {
            "-h" | "--help" => {
                usage();
                std::process::exit(1);
            }
            _ => input = arg.to_string(),
        }
    }

    println!("{}", eval(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_scan() {
        {
            let expected = vec!["1", "+", "1"];
            let actual = scan("1 + 1");

            assert_eq!(expected, actual);
        }
        {
            let expected = Vec::<String>::new();
            let actual = scan("");

            assert_eq!(expected, actual);
        }
        {
            let expected = vec!["1", "+", "2", "*", "3", "+", "(", "3.4", "/", "3", ")"];
            let actual = scan("1 + 2 * 3 + (3.4 / 3)");

            assert_eq!(expected, actual);
        }
    }

    #[test]
    #[should_panic]
    fn panic_scan() {
        scan("panic!");
    }

    #[test]
    #[should_panic]
    fn another_panic_scan() {
        scan("1 + 2 | 3");
    }

    #[test]
    fn good_infix_to_postfix() {
        {
            let expected = vec!["1", "1", "+"];
            let actual = infix_to_postfix(scan("1 + 1"));

            assert_eq!(expected, actual);
        }
        {
            let expected = vec!["1", "2", "3", "*", "+"];
            let actual = infix_to_postfix(scan("1 + 2 * 3"));

            assert_eq!(expected, actual);
        }
        {
            let expected = vec!["1", "1", "+", "2", "*", "4", "2", "/", "-"];
            let actual = infix_to_postfix(scan("(1 + 1) * 2 - 4 / 2"));

            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn solve_based_on_precedence() {
        {
            let expected = 5;
            let actual = eval("1 + 2 * 2") as i32;

            assert_eq!(expected, actual);
        }
        {
            let expected = 1;
            let actual = eval("2 + 2 - 3") as i32;

            assert_eq!(expected, actual);
        }
        {
            let expected = 5;
            let actual = eval("1 + 2 * (1 + 1)") as i32;

            assert_eq!(expected, actual);
        }
        {
            let expected = 1;
            let actual = eval("1 * (2 + 2) - 3") as i32;

            assert_eq!(expected, actual);
        }
    }
}
